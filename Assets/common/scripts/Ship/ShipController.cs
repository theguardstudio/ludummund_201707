﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumTypes;
public class ShipController : MonoBehaviour {



    /*  */

    public void AA_StartThrust()
    {
        m_thrusting = true;
        m_shipEvents.m_shipAnimator.SetTrigger("Run");
        //TODO: call to ship.starthrustFX (minimo sonido y animacion)
    }

    public void AA_StopThrust ()
    {
        m_thrusting = false;
        m_shipEvents.m_shipAnimator.SetTrigger("GoToIdle");
        //TODO: call to ship.StophrustFX (minimo sonido y animacion)
    }
    public void AA_StartRotate(RotationType _rotationType)
    {
        if (_rotationType == RotationType.Left)
        {
            m_shipEvents.m_shipAnimator.SetTrigger("TurnLeft");
            m_rotateLeft = true;

        }
        if (_rotationType == RotationType.Right)
        {
            m_shipEvents.m_shipAnimator.SetTrigger("TurnRight");
            m_rotateRight = true;
        }

    }

    public void AA_StopRotate (RotationType rotationToStop)
    {
        if (rotationToStop == RotationType.Left)
        {
            m_rotateLeft = false;

        }
        if (rotationToStop == RotationType.Right)
        {
            m_rotateRight = false;
        }

        m_shipEvents.m_shipAnimator.SetTrigger("GoToIdle");

    }


  

    public void AA_FireCurrentWeapon()
    {

        if (AA_TryToUsePower(m_repulsorProyectileCost))
        {
            m_shipFireController.AA_FireSelectedWeapon();
        } else
        {
            //TODO: SOUND AND VISUAL FX FOR 'CANT FIRE MISSILES'
        }
    }

    public void AA_SensorPing()
    {
        Debug.Log("PING");

    }

    public void AA_StartEmergencyStop()
    {
        m_emergencyStop = true;
    }

    public void AA_EndEmergencyStop()
    {
        m_emergencyStop = false;
    }

    public bool AA_TryToUsePower (float powerRequired, bool dieIfCant = false)
    {
        if (powerRequired < m_currentPower)
        {
            m_currentPower -= powerRequired;

            AA_UpdateEnergyCanvas();
            return true;
        } else //if (dieIfCant)
        {
            m_currentPower = 0;
            m_gameController.AA_ChangeGameStatus(GameStatus.GameOver);
            m_shipEvents.AA_PerformDeath();
            Debug.Log(" -- end game now --");
        }

        //TODO: else if (dieIfCant) call to GameManager.EndGame()
        return false;
    }


    #region Actualizaciones en update
    private void AA_CheckForThrust ()
    {
        if (m_thrusting)
        {
            if (AA_TryToUsePower(m_thrustCostPerTick * Time.deltaTime))
            {
                //TODO: call to m_spaceShip.Thrust (); with time.deltatime;
                m_spaceShip.Accelerator();
               // Debug.Log("--- thrusting ----");
            }
            else
            {
                m_thrusting = false;
            }
        }

        if (m_unthrusting)
        {

            if (AA_TryToUsePower(m_emergencyStopCostPertTick * Time.deltaTime))
            {
                //TODO: call to m_spaceShip.Thrust (); with time.deltatime;
                //Debug.Log("--- thrusting ----");
            }
            else
            {
                m_unthrusting = false;
            }
        }
    }

    private void AA_CheckForRotate()
    {
        if (m_rotateLeft)
        {
            if (AA_TryToUsePower(m_rotateCostPerTick * Time.deltaTime))
            {
                m_spaceShip.Rotator();
            }
            else
            {
                m_rotateLeft = false;
            }
        }

        if (m_rotateRight)
        {
            if (AA_TryToUsePower(m_rotateCostPerTick * Time.deltaTime))
            {
                m_spaceShip.Rotator();
                // Debug.Log("--- thrusting ----");
            }
            else
            {
                m_rotateRight = false;
            }
        }

    }

    private void AA_CheckForStop ()
    {
        if (m_emergencyStop)
        {
            if (AA_TryToUsePower(m_emergencyStopCostPertTick * Time.deltaTime))
            {
                m_spaceShip.Accelerator();
            }
            else
            {
                m_emergencyStop = false;
            }

        }
    }

    #endregion


    private void AA_UpdateEnergyCanvas ()
    {
        m_energyCanvasController.AA_UpdateDisplayedValue(m_currentPower);
    }

	private void AA_EngineSound(){
        if (m_gameController.gameStatus != GameStatus.Playing)
            return;
		float volume = Mathf.Max (Mathf.Abs(Input.GetAxis("Vertical")),Mathf.Abs(Input.GetAxis("Horizontal")));
		m_engines.volume = Mathf.Clamp(volume,0.0f,0.15f);
	}

	private void Update(){
		AA_EngineSound ();
	}

    private void FixedUpdate()
    {
        AA_CheckForThrust();
        AA_CheckForRotate();
        AA_CheckForStop();
        m_spaceShip.Stabilizator();

    }

    private void Start()
    {
        AA_UpdateEnergyCanvas();
    } 
    private void Awake()
    {
        m_spaceShip = FindObjectOfType<SpaceShip>();
        m_shipFireController = FindObjectOfType<ShipFireController>();
        m_energyCanvasController = FindObjectOfType<GameController>().m_inGameGui as EnergyCanvasController;
        m_engines = m_spaceShip.engineAudioSource;
        m_gameController = GetComponent<GameController>();
        m_shipEvents = FindObjectOfType<ShipEvents>();
    }


    


    [Header ("For report only")]
    [SerializeField]
    bool m_thrusting = false;
    [SerializeField]
    bool m_unthrusting = false;
    [SerializeField]
    bool m_rotateRight = false;
    [SerializeField]
    bool m_rotateLeft = false;
    [SerializeField]
    bool m_emergencyStop = false;
    //RotationType m_rotation = RotationType.None;
    

    [Space(20)]
    [Header ("Setable parameters")]
    [SerializeField]
    float m_maxPower = 50;
    [SerializeField]
    float m_currentPower = 50;

    [Space(15)]
    [SerializeField]
    float m_thrustCostPerTick = 1;
    [SerializeField]
    float m_rotateCostPerTick = 1;
    [SerializeField]
    float m_repulsorProyectileCost = 5;
    [SerializeField]
    float m_sensorPingCost = 2;
    [SerializeField]
    float m_emergencyStopCostPertTick = 2;
    [SerializeField]
    public float m_shieldCostPerHit = 30;

    //keep hidden
    SpaceShip m_spaceShip;
    ShipFireController m_shipFireController;
    EnergyCanvasController m_energyCanvasController;
	public AudioSource m_engines;
    GameController m_gameController;
    ShipEvents m_shipEvents;


    

}
