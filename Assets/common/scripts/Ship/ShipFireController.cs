﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipFireController : MonoBehaviour {


    public void AA_FireSelectedWeapon ()
    {
        //WeaponSystem newWeapon = Instantiate(m_currentSelectedWeapon, m_references.m_missileSpawnPoint.transform.position, m_references.m_missileSpawnPoint.transform.rotation);
        if (Time.time >= canFireAtSecond)
        {
            WeaponSystem newWeapon = Instantiate(m_currentSelectedWeapon);
            newWeapon.AA_InitMissile(m_references.m_missileSpawnPoint.transform.position, m_references.m_missileSpawnPoint.transform.rotation, m_references.m_shipRigidbody);
            canFireAtSecond = Time.time + m_currentSelectedWeapon.m_cooldownTime;
        } else
        {
            //TODO: play 'weapon not ready' sound and FX
        }
    }




    private void Awake()
    {
       m_currentSelectedWeapon = m_references.m_missileRepulsor;
    }





    [SerializeField]
    ShipFirecontrollerReferences m_references;

    float canFireAtSecond = 0;

    WeaponSystem m_currentSelectedWeapon;






    

    [System.Serializable]
    class ShipFirecontrollerReferences
    {
       public WeaponSystem m_missileRepulsor;
       //public WeaponSystem m_missileRepulsor;

        public GameObject m_missileSpawnPoint;
        public Rigidbody2D m_shipRigidbody;
    }
}
