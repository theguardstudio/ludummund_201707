﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsReactor : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // if (collision.gameObject.tag == "Collisionable" && OnCollision != null)
        //{
        if (OnCollision != null)
        {
            collidedGO = collision.gameObject;
            OnCollision();
        }
        //}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (OnTrigger != null)
        {
            triggeredGO = collision.gameObject;
            OnTrigger();
        }

    }

    public GameObject collidedGO;
    public delegate void CollisionHappens();
    public event CollisionHappens OnCollision;


    public GameObject triggeredGO;
    public delegate void TriggerHappens();
    public event TriggerHappens OnTrigger;
}
