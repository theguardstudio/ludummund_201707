﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//estoy y el ShipPhysicsReactor es un poco overkill para algo tan sencillo pero es el experimento que te comentaba al empezar que queria probar :)
public class ShipEvents : MonoBehaviour {
    public void AA_PerformDeath()
    {
        m_shipSprite.color = m_deathColor;
        m_navigationHelper.SetActive(false);
        m_shieldAnimator.Play("ShieldIdle");
        m_shipAnimator.Play("Idle");
        m_shieldRenderer.enabled = false;
        shield_sound.Stop();
        m_engineAudioSource.Stop();

    }
    public void AA_AnimationJumpToWarp()
    {
        m_navigationHelper.SetActive(false);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        m_shipController.m_engines.Stop();
        m_shieldCollider.enabled = false;
        m_shipAnimator.SetTrigger("EnterWarp");

    }
    void AA_AnimationShieldsUp ()
    {
        if (m_shipPhysicsReactor.collidedGO.tag != "Asteroid")
            return;

        m_shieldAnimator.Play("ShieldIdle");
        m_shieldAnimator.SetTrigger("ShieldsUp");

		shield_sound.Play ();
    }

    void EnergyDrain ()
    {
        if (m_shipPhysicsReactor.collidedGO.tag != "Asteroid")
            return;

        m_shipController.AA_TryToUsePower(m_shipController.m_shieldCostPerHit, true);
    }

    void Update ()
    {
        if (m_thisSceneWarp == null)
            return;

        Vector3 arrowDirection = (m_thisSceneWarp.transform.position - m_arrowHolder.transform.position).normalized;
        Vector3 newDir = Vector3.RotateTowards(m_arrowHolder.transform.forward, arrowDirection, 4 , 0f);
        m_arrowHolder.transform.rotation = Quaternion.LookRotation(newDir);
    }

    private void OnEnable()
    {
        m_shipPhysicsReactor.OnCollision += AA_AnimationShieldsUp;
        m_shipPhysicsReactor.OnCollision += EnergyDrain;


    }
    private void OnDisable()
    {
        m_shipPhysicsReactor.OnCollision -= AA_AnimationShieldsUp;
        m_shipPhysicsReactor.OnCollision -= EnergyDrain;
    }


    private void Awake()
    {
        m_shipController = FindObjectOfType<ShipController>();
        m_thisSceneWarp = FindObjectOfType<WarpController>();
    }
    [SerializeField]
    SpriteRenderer m_shieldRenderer;
    [SerializeField]
    AudioSource m_engineAudioSource;
    [SerializeField]
    Color m_deathColor;
    [SerializeField]
    SpriteRenderer m_shipSprite;
    [SerializeField]
    GameObject m_navigationHelper;
	[SerializeField]
	AudioSource shield_sound;
	[SerializeField]
    Animator m_shieldAnimator;
    [SerializeField]
    public Animator m_shipAnimator;
    [SerializeField]
    PhysicsReactor m_shipPhysicsReactor;
    [SerializeField]
    Collider2D m_shieldCollider;
    [SerializeField]
    GameObject m_arrowHolder;
    ShipController m_shipController;
    public
    WarpController m_thisSceneWarp;


}
