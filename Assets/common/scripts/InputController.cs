﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumTypes;

public class InputController : MonoBehaviour {

    private void Update()
    {


         //m_gameController.AA_ReceiveKey(InputAction.Thruster, Input.GetAxis("Vertical"));

        if (Input.GetKeyDown("up")  || Input.GetKeyDown("w") )
        {
            m_gameController.AA_ReceiveAction(InputAction.Up);
        }

        if (Input.GetKeyUp("up") || Input.GetKeyUp("w") )
        {
            m_gameController.AA_ReceiveAction(InputAction.EndUp);
        }

        if (Input.GetKeyDown("down") || Input.GetKeyDown("s"))
        {
            m_gameController.AA_ReceiveAction(InputAction.Down);
        }

        if ( Input.GetKeyUp("down") ||  Input.GetKeyUp("s"))
        {
            m_gameController.AA_ReceiveAction(InputAction.EndDown);
        }


        if (Input.GetKeyDown("left")|| (Input.GetKeyDown("a")))
        {
            m_gameController.AA_ReceiveAction(InputAction.StartLeft);
        }

        if (Input.GetKeyUp("left")||Input.GetKeyUp("a"))
        {
            m_gameController.AA_ReceiveAction(InputAction.StopLeft);
        }

        if (Input.GetKeyDown("right")|| Input.GetKeyDown("d"))
        {
            m_gameController.AA_ReceiveAction(InputAction.StartRight);
        }

        if (Input.GetKeyUp("right")|| Input.GetKeyUp("d"))
        {
            m_gameController.AA_ReceiveAction(InputAction.StopRight);
        }

        if (Input.GetKeyDown (KeyCode.Space))
        {
            m_gameController.AA_ReceiveAction(InputAction.Action1);
        }

        



    } 

    [SerializeField]
    GameController m_gameController;
}
