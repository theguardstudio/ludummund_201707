﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EnumTypes
{
    public enum GameStatus { Undefined, InMenu, Playing, GameOver, WarpPoint, GameWon, InHelp, InCredits };

    public enum InputAction { Up,Down,StartLeft,StopLeft,StartRight,StopRight, Action1, EndUp, EndDown }

    public enum RotationType { None, Left, Right}
}