﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EnumTypes;
public class WarpController : MonoBehaviour {


    private void GoToNextScene ()
    {
        if (m_physicsReactor.triggeredGO.tag != "Player")
            return;

        FindObjectOfType<ShipEvents>().AA_AnimationJumpToWarp();
        if (m_isGameTarget)
        {
            //TODO: play victory music and other effect??
            m_gameController.AA_ChangeGameStatus(GameStatus.GameWon);
        }
        else
        {
            m_gameController.AA_ChangeGameStatus(GameStatus.WarpPoint);
            
            m_gameController.nextScene = nextScene;                                             //  m_gameController.AA_PerformWarppointActions(nextScene);
        }                                                                                 //SceneManager.LoadScene(nextScene);
    }


    private void OnEnable()
    {
        m_physicsReactor.OnTrigger += GoToNextScene;

    }

    private void OnDisable()
    {
        m_physicsReactor.OnTrigger -= GoToNextScene;
    } 

    void Awake()
    {
        m_gameController = FindObjectOfType<GameController>();
    }

    [SerializeField]
    string nextScene;

    [SerializeField]
    PhysicsReactor m_physicsReactor;

    [SerializeField]
    bool m_isGameTarget;
    private GameController m_gameController;
}
