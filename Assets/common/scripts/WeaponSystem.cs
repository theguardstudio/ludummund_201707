﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSystem : MonoBehaviour {




    public void AA_InitMissile (Vector2 position, Quaternion rotation, Rigidbody2D shipRigidbody)
    {


       
        transform.position = position;
        transform.rotation = rotation;


        m_references.m_weaponRigidbody.rotation = shipRigidbody.rotation;
        m_references.m_weaponRigidbody.velocity = new Vector2 (transform.up.x, transform.up.y) * m_missileInitialSpeed;
        if (m_inheritShipPhysics)
        {
            m_references.m_weaponRigidbody.velocity += shipRigidbody.velocity;
        }


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
         if (collision.gameObject.tag == "Asteroid")
        {
            Debug.Log("-- perform explosion --");
            m_references.m_weaponRigidbody.velocity = Vector2.zero;
            m_references.m_weaponRigidbody.isKinematic = true;
            m_references.m_collider.enabled = false;
            m_references.m_animator.SetTrigger("Explode");

        }
    }





    private void FixedUpdate()
    {
        m_references.m_weaponRigidbody.AddForce(transform.up * m_missileAcceleration, ForceMode2D.Force );
    }



    private void Start()
    {
		Destroy (gameObject,m_lifetime);
    }

 

	public float m_cooldownTime = .5f;
    public float m_missileInitialSpeed = 1;
    public float m_missileAcceleration = 1;

    [SerializeField]
    bool m_inheritShipPhysics = false;
    [SerializeField]
    WeaponSystemReferences m_references;

    public float m_lifetime = 3;




    [System.Serializable]
    class WeaponSystemReferences
    {
        public Rigidbody2D m_weaponRigidbody;
        public Animator m_animator;
        public Collider2D m_collider;
    }

} 
