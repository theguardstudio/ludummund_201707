﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnumTypes;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {



    public void AA_ReceiveAction(InputAction inputAction)
    {
        switch (gameStatus)
        {
            case GameStatus.Playing:
                AA_RunPlayingInput(inputAction);
                break;

            case GameStatus.WarpPoint:
                AA_RunWarppointInput(inputAction);
                break;

            case GameStatus.GameOver:
                AA_RunGameOverInput(inputAction);
                break;

            case GameStatus.GameWon:
                AA_RunGameWonInput(inputAction);
                break;
        }

    }




    private void AA_RunPlayingInput(InputAction inputAction)
    {
        switch (inputAction) {
            case InputAction.Up:
                m_shipController.AA_StartThrust();
                break;
            case InputAction.EndUp:
                m_shipController.AA_StopThrust();
                break;
            case InputAction.Down:
                m_shipController.AA_StartEmergencyStop();
                break;
            case InputAction.EndDown:
                m_shipController.AA_EndEmergencyStop();
                break;
            case InputAction.StartLeft:
                m_shipController.AA_StartRotate(RotationType.Left);
                break;
            case InputAction.StopLeft:
                m_shipController.AA_StopRotate(RotationType.Left);
                break;
            case InputAction.StartRight:
                m_shipController.AA_StartRotate(RotationType.Right);
                break;
            case InputAction.StopRight:
                m_shipController.AA_StopRotate(RotationType.Right);
                break;
            case InputAction.Action1:
                m_shipController.AA_FireCurrentWeapon();
                break;
        }
    }

    private void AA_RunWarppointInput(InputAction inputAction)
    {
        switch (inputAction)
        {
            case InputAction.Action1:
                SceneManager.LoadScene(nextScene);
                break;
        }
    }

    private void AA_RunGameOverInput(InputAction inputAction)
    {
        switch (inputAction)
        {
            case InputAction.Action1:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
                break;
        }
    }

    private void AA_RunGameWonInput(InputAction inputAction)
    {
        switch (inputAction)
        {
            case InputAction.Action1:
                SceneManager.LoadScene("scene_menu");
                break;
        }
    }
    public void AA_ChangeGameStatus(GameStatus newStatus)
    {
        m_currentGui.AA_HideMenu();

        switch (newStatus)
        {
            case GameStatus.InMenu:
                gameStatus = GameStatus.InMenu;
                m_currentGui = m_mainMenuGui;
                break;
            case GameStatus.Playing:
                gameStatus = GameStatus.Playing;
                m_currentGui = m_inGameGui;
                break;
            case GameStatus.GameOver:
                gameStatus = GameStatus.GameOver;
                m_currentGui = m_gameOverGui;
                break;
            case GameStatus.WarpPoint:
                gameStatus = GameStatus.WarpPoint;
                m_currentGui = m_goToNextGui;
                break;
            case GameStatus.GameWon:
                gameStatus = GameStatus.GameWon;
                m_currentGui = m_gameWinGui;
                break;
            case GameStatus.InCredits:
                gameStatus = GameStatus.InCredits;
                m_currentGui = m_credits;
                break;
            case GameStatus.InHelp:
                gameStatus = GameStatus.InHelp;
                m_currentGui = m_menuControls;
                break;

        }

        m_currentGui.AA_ShowGui();

    }



    private void Start()
    {
        Debug.Log("---" + SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name == "scene_menu")
        {
            m_currentGui = m_inGameGui;
            AA_ChangeGameStatus(GameStatus.InMenu);
        }
        else
        {

            m_currentGui = m_mainMenuGui;
            AA_ChangeGameStatus(GameStatus.Playing);
        }
    } 


    public GameStatus gameStatus = GameStatus.Undefined;
    [SerializeField]
    public ShipController m_shipController;
    public string nextScene;


    [Header("References to guis (D&D)")]
    [SerializeField]
    BaseGuiController m_mainMenuGui;
    [SerializeField]
    public BaseGuiController m_inGameGui;
    [SerializeField]
    BaseGuiController m_gameOverGui;
    [SerializeField]
    BaseGuiController m_goToNextGui;
    [SerializeField]
    BaseGuiController m_gameWinGui;
    [SerializeField]
    BaseGuiController m_menuControls;
    [SerializeField]
    BaseGuiController m_credits;


    [SerializeField]
    private BaseGuiController m_currentGui;

    public object Scenemanager { get; private set; }
}
