﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateArround : MonoBehaviour {
	public Transform target;
	public float rotation_speed;
	private Vector3 z_axis = new Vector3(0.0f,0.0f,1.0f);
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (target.position, z_axis, rotation_speed * Time.deltaTime);
	}
}
