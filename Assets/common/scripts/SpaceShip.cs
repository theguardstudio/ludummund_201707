﻿using UnityEngine;
using System.Collections;


public class SpaceShip : MonoBehaviour
{
	public float speed_foward = 5.0f;
	public float speed_rotation  = 2.0f;
	public float stabilization_threshold = 0.5f;
	public float max_velocity = 10.0f;
	public float rotation_threshold = 10.0f;
    public float maxAngularVelocity = 25;

    public AudioSource engineAudioSource;

   
	/*I WONT USE THE FUCKIING SOURCETREE CLIENT AGAIN*/


	public void Stabilizator(){
		if(Input.GetAxis("Vertical")== 0 && GetComponent<Rigidbody2D> ().velocity.magnitude < stabilization_threshold)
			GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		if (Input.GetAxis ("Horizontal") == 0 && Mathf.Abs (GetComponent<Rigidbody2D> ().angularVelocity) < rotation_threshold)
			GetComponent<Rigidbody2D> ().angularVelocity = 0;
	}

	public void Rotator(){
        GetComponent<Rigidbody2D>().AddTorque(speed_rotation * (-1 * Input.GetAxis("Horizontal")));
        GetComponent<Rigidbody2D>().angularVelocity = Mathf.Clamp(GetComponent<Rigidbody2D>().angularVelocity, -maxAngularVelocity, maxAngularVelocity);
		//if(GetComponent<Rigidbody2D>().angularVelocity >)

    }

	public float Velocimeter(){
		return GetComponent<Rigidbody2D> ().velocity.magnitude;
	}

	public void Accelerator(){
		if(Input.GetAxis("Vertical")>0)
			GetComponent<Rigidbody2D> ().AddForce(transform.up * Input.GetAxis ("Vertical") * speed_foward);
		else
			GetComponent<Rigidbody2D> ().AddForce(GetComponent<Rigidbody2D>().velocity * Input.GetAxis ("Vertical") * speed_foward);
	}
}
