﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EnumTypes;
public class MainMenuController : BaseGuiController {

    public void AA_ClickOnPlay ()
    {

        SceneManager.LoadScene("scene0");
    }

    public void AA_ClickOnControls ()
    {
        m_gameController.AA_ChangeGameStatus(GameStatus.InHelp);
    }

    public void AA_ClickOnCredits ()
    {
        m_gameController.AA_ChangeGameStatus(GameStatus.InCredits);
    }



}
