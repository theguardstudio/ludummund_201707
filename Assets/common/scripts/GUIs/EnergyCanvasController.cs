﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnergyCanvasController : BaseGuiController {



    public void AA_UpdateDisplayedValue (float newValue)
    {
        energyValue.text = newValue.ToString("###.#") + " %";
    }

    [SerializeField]
    Text energyValue;
}
