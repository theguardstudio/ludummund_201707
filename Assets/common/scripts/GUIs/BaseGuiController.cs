﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGuiController : MonoBehaviour {

    public void AA_ShowGui()
    {
        gameObject.SetActive(true);
    }

    public void AA_HideMenu ()
    {
        gameObject.SetActive(false);
    }

    public void AA_ClickOnBackToMain ()
    {
        m_gameController.AA_ChangeGameStatus(EnumTypes.GameStatus.InMenu);
    }
    public void Start()
    {
        //uncomment this for build
        //AA_HideMenu();

        
    }

    private void Awake()
    {
        m_gameController = GetComponentInParent<GameController>();
    } 
    protected GameController m_gameController;
}
